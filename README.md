# Messing around with git scripts
Sometimes these need to be somewhere on your path or somewhere in a git repo. Maintaining the sources here and symlinking to this directory from wherever they need to be (eg C:\\Program Files\Git\usr\bin)

## git-backup

Clones a list of repos locally. Replaces previous local repos of the same name (ie removes the previous backup once the latest version is successfully cloned).

## git-go
Playing around with git scripts
Should be symlinked from somewhere on the path
Makes the command `git go` available from any git repo

## git-email
list emails you commonly use for git config user.email

## git-open
for saving a shortcut to the repo platform

1. Set the URL which will be opened (ie the URL to the remote repo platform, such as gitlab):
`git setopen https://git.bluefinsolutions.com/sbell/demo`

2. Open the repo from the local repo
`git open`
 should open the URL in the browser

Requires two scripts: setopen and open
