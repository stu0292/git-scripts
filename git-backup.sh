#!/usr/bin/env bash

# Clones a list of repos locally for backup purposes.
# Folders with the same name as the local repo (ie previous backups) are deleted

# gitBackup https://user:password@repo.com/path localRepoName
function gitBackup(){
  # return early if local repo name not provided
  if [ -z "$2" ]; then
       echo "Please provide second arg for local repo name"
       return
  fi

  # clone the repo into a temp folder
  git clone $1 "${2}-temp" 
  # remove the old repo
  rm -rf $2
  # rename the temp repo to the new one
  mv "${2}-temp" $2
}

# working directory
cd "C:\Users\user1234\RepoBackups"

# list of repos to backup
gitBackup https://user:gt4pzgvimaclq@dev.azure.com/group/project/_git/myrepo myrepo

 read -s -n 1 -p "Press any key to continue"

